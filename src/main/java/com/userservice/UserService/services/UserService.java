package com.userservice.UserService.services;

import com.userservice.UserService.dtos.OrderDTO;
import com.userservice.UserService.dtos.UserDTO;
import com.userservice.UserService.entities.UserEntity;
import com.userservice.UserService.repositories.UserRepository;
import com.userservice.UserService.utils.Validations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${order-service.base-url}")
    private String orderServiceBaseUrl;

    @Value("${order-service.order-url}")
    private String orderServiceOrderUrl;

    @Autowired
    private RestTemplateBuilder restTemplate;

    @Autowired // Autowired's annotation automatically crates the object of the annotated class.
    private UserRepository repository;


    /**
     * This method is used to get all user data from the DB
     *
     */
    public List<UserDTO> getAllUsers(){
        LOGGER.info("================================ Enter into getAllUsers() in UserService ================================");
        List<UserDTO> users = null;
        try {
            users = repository.findAll()
                    .stream()
                    .map(userEntity -> new UserDTO(
                            userEntity.getId().toString(),
                            userEntity.getName(),
                            userEntity.getAge()
                    )).collect(Collectors.toList());
        }
        catch (Exception e){
            LOGGER.warn("-----> Exception in UserService -> getAllUsers() - "+e);
        }
        return users;
    }


    /*
     * ======================================================================================
     * This method is used to get order data relevant to a user using OrderService's API
     *
     */
    public List<OrderDTO> getOrdersByUserId(Long id){
        LOGGER.info("================================ Enter into getOrdersByUserId() in UserService ================================");

        List<OrderDTO> orders = null;

        try{
            orders = restTemplate.build().getForObject(
                    orderServiceBaseUrl.concat(orderServiceOrderUrl).concat("/"+id),
                    List.class
            );
        }
        catch (Exception e){
            LOGGER.warn("================================ Exception in UserService -> getOrdersByUserId() - "+e);
        }

        return orders;
    }

    /*
     * =====================================================================================
     * This method is used to save new user to the database
     *
     */
    public boolean saveUser(UserDTO user){
        LOGGER.info("================================ Enter into saveUser() in UserService ================================");


        try{
            if(Validations.validateNewUserData(user)){
                UserEntity newUserEntity = new UserEntity(user.getName(),user.getAge());
                repository.save(newUserEntity);
                return  true;
            }
            else {
                return false;
            }
        }
        catch (Exception e){
            LOGGER.warn("================================ An exception occurred in saveUser() : "+e.getMessage());
            return false;
        }
    }

    /*
     * ========================================================================================
     * This method is used to update the existing user data
     *
     */
    public boolean updateUser(Long id, UserDTO userDTO){
        LOGGER.info("================================ Enter into updateUser() in UserService ================================");

        try {
            Optional<UserEntity> requestedUser = repository.findById(id);

            if(requestedUser.isPresent()){
                UserEntity currentUser = requestedUser.get();

                if(userDTO.getName() != null && userDTO.getAge() != null){ // if name and age both needed to be updated

                    currentUser.setName(userDTO.getName());
                    currentUser.setAge(userDTO.getAge());
                    repository.save(currentUser);
                    return true;
                }
                else if (userDTO.getName() == null) { // if only age needed to be updated

                    currentUser.setAge(userDTO.getAge());
                    repository.save(currentUser);
                    return true;
                }
                else if(userDTO.getAge() == null){ // if only name needed to be updated
                    currentUser.setName(userDTO.getName());
                    repository.save(currentUser);
                    return true;
                }
                else {
                    return false;
                }
            }
            else{
                LOGGER.info("================================ updateUser() No user was found for the requested ID :"+id);
                return false;
            }
        }
        catch (Exception e){
            LOGGER.warn("================================ An exception occurred in updateUser() : "+e.getMessage());
            return false;
        }
    }

    /*
     * ========================================================================================
     * This method is used to check whether requested user exist on DB
     *
     */
    public boolean checkUserExist(Long id){
        LOGGER.info("================================ Enter into checkUserExist() in UserService ================================");
        try{
            Optional<UserEntity> requiredUser = repository.findById(id);
            return requiredUser.isPresent();
        }
        catch (Exception e){
            LOGGER.warn("================================ An exception occurred in updateUser() : "+e.getMessage());
            return false;
        }
    }
}
