package com.userservice.UserService.entities;

import javax.persistence.*;

@Entity
@Table(name = "user") // annotation to table name
public class UserEntity {


    @Id // annotation to mention the primary key of the table
    @GeneratedValue(strategy = GenerationType.IDENTITY) // to auto increment the ID
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String age;

    public UserEntity(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public UserEntity(Long id, String name, String age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public UserEntity() {
    }

    public Long getId() { return id;}

    public void setId(Long id) { this.id = id;}

    public String getName() { return name;}

    public void setName(String name) { this.name = name;}

    public String getAge() {return age;}

    public void setAge(String age) {this.age = age;}
}
