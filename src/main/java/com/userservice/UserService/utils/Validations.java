package com.userservice.UserService.utils;

import com.userservice.UserService.dtos.UserDTO;

public class Validations {

    /*
        ==============================================================================================
        This method is used to validate new user data
     */
    public static boolean validateNewUserData(UserDTO user){

        if(user.getAge() != null && user.getName() != null){
            return Integer.parseInt(user.getAge()) > 0 && Integer.parseInt(user.getAge()) < 120; // check age is in acceptable range
        }
        else{
            return false;
        }
    }
}
