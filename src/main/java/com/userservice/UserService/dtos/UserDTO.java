package com.userservice.UserService.dtos;

public class UserDTO {
    private String id;
    private String name;
    private String age;


    // Default constructor
    public UserDTO(){

    }

    public UserDTO(String id,String name, String age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public UserDTO(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public String getAge() {
        return age;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
