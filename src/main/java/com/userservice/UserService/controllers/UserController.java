package com.userservice.UserService.controllers;
/*
 *This controller is responsible for managing all the api
 * requests with respect to the user
 */

import com.userservice.UserService.dtos.OrderDTO;
import com.userservice.UserService.dtos.UserDTO;
import com.userservice.UserService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    /*
     * ===================================================================
     * This method is used to return the all user data.
     */
    @GetMapping("/getAll")
    public List<UserDTO> getAllUsers(){
        return userService.getAllUsers();
    }


    /*
     * ==================================================================
     *  This method is used to return all orders filtered by user id
     */
    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final Long id){

        return userService.getOrdersByUserId(id);
    }

    /*
     * ==================================================================
     * This method is used to save new user
     */
    @PostMapping("/save")
    public boolean saveUser(@RequestBody UserDTO userDTO){
        return userService.saveUser(userDTO);

    }

    /*
     * ===================================================================
     *  This method is used to update user data
     */
    @PutMapping("/updateUser/{id}")
    public boolean updateUser(@RequestBody UserDTO userDTO, @PathVariable final Long id){
       return userService.updateUser(id,userDTO);
    }

    /*
     * ===================================================================
     * This method is used to check requested user was existed in the DB
     *
     */
    @GetMapping("/checkUser/{id}")
    public boolean checkUserExist(@PathVariable final Long id){
        return userService.checkUserExist(id);
    }

}
